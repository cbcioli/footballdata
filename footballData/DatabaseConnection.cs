﻿using System;
using System.Data;
using System.Configuration;
using MySql.Data.MySqlClient;
using System.Collections.Generic;

namespace footballData
{
    class DatabaseConnection
    {
        private MySqlConnection mDbConnection;

        public DatabaseConnection()
        {
            mDbConnection = new MySqlConnection();
            mDbConnection.ConnectionString = ConfigurationManager.ConnectionStrings["localData"].ConnectionString;
        }

        public MySqlConnection GetConnection()
        {
            return mDbConnection;
        }

        public void Open()
        {
            try
            {
                mDbConnection.Open();
            }
            catch (MySqlException ex)
            {
                throw new Exception("Cannot connect to server.", ex);
            }
        }

        public void Close()
        {
            try
            {
                if (mDbConnection.State == ConnectionState.Open)
                {
                    mDbConnection.Close();
                }
            }
            catch (MySqlException ex)
            {
                throw new Exception("Cannot disconnect from sever.", ex);
            }
        }
    }
}
