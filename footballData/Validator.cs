﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace footballData
{
    class Validator
    {
        public static int ValidateIntEntered(string inputString, string requiredValue)
        {
            int outputInt;
            while (!int.TryParse(inputString, out outputInt))
            {
                Console.WriteLine($"You haven't entered a valid value. Please enter an integer for {requiredValue}.");
                inputString = Console.ReadLine();
            }
            return outputInt;
        }

        public static int ValidateIntEntered(string inputString, string requiredValue, int min)
        {
            int outputInt;
            while (!int.TryParse(inputString, out outputInt) || outputInt < min)
            {
                Console.WriteLine($"You haven't entered a valid value for {requiredValue}. Please enter an integer of at least {min}.");
                inputString = Console.ReadLine();
            }
            return outputInt;
        }

        public static int ValidateIntEntered(string inputString, string requiredValue, int min, int max)
        {
            int outputInt;
            while(!int.TryParse(inputString, out outputInt) || (outputInt < min || outputInt > max))
            {
                Console.WriteLine($"You haven't entered a valid value for {requiredValue}. Please enter an integer between {min} and {max}.");
                inputString = Console.ReadLine();
            }
            return outputInt;
        }

        public static decimal ValidateDecimalEntered(string inputString, string requiredValue)
        {
            decimal outputDecimal;
            while(!decimal.TryParse(inputString, out outputDecimal))
            {
                Console.WriteLine($"You haven't entered a valid value. Please enter a decimal for {requiredValue}.");
                inputString = Console.ReadLine();
            }
            return outputDecimal;
        }

        public static string ValidateStringEntered(string inputString, string requiredValue)
        {
            while(string.IsNullOrWhiteSpace(inputString))
            {
                Console.WriteLine($"It appears you left this blank! Please enter {requiredValue}.");
                inputString = Console.ReadLine();
            }
            return inputString;
        }

        public static float ValidateFloatEntered(string inputString, string requiredValue)
        {
            float output;
            while(!float.TryParse(inputString, out output))
            {
                Console.WriteLine($"You haven't entered a valid value. Please enter a float for {requiredValue}.");
                inputString = Console.ReadLine();
            }
            return output;
        }
    }
}
