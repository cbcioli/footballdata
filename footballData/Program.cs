﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;

namespace footballData
{
    class Program
    {
        static void Main(string[] args)
        {

            bool isRunning = true;
            MenuAction menuAction = new MenuAction();

            while (isRunning)
            {
                Console.Clear();

                Console.WriteLine("Welcome to English Premier League Football Data 2016/2017!");
                Console.WriteLine("Please choose from the following options:");
                Console.WriteLine("1. Final Table");
                Console.WriteLine("2. Team Shot Percentages");
                Console.WriteLine("3. Team Yellow Cards");
                Console.WriteLine("4. Team Red Cards");
                Console.WriteLine("5. Team Corners");
                Console.WriteLine("5. Exit");
                string userSelectionInput = Console.ReadLine();
                string userSelection = Validator.ValidateStringEntered(userSelectionInput, "a valid menu option").ToLower();

                switch(userSelection)
                {
                    case "1":
                    case "final table":
                        menuAction.ShowFinalTable();
                        break;
                    case "2":
                    case "team shot percentages":
                        menuAction.ShowShotsTable();
                        break;
                    case "3":
                    case "team yellow cards":
                        menuAction.ShowCardsTable("yellow");
                        break;
                    case "4":
                    case "team red cards":
                        menuAction.ShowCardsTable("red");
                        break;
                    case "5":
                    case "team corners":
                        menuAction.ShowCornersTable();
                        break;
                    case "6":
                    case "exit":
                        isRunning = false;
                        break;
                    default:
                        Console.WriteLine("Please enter a valid menu option.");
                        break;
                }
                Console.WriteLine("Press any key to continue...");
                Console.ReadKey();
            }
        }
    }
}
