﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace footballData
{
    class Standing
    {
        public int mStanding;
        public string mTeamName;
        public int mMatchesPlayed;
        public int mWins;
        public int mDraws;
        public int mLosses;
        public int mGoalsFor;
        public int mGoalsAgainst;
        public int mGoalDifferential;
        public int mPoints;
        public int mShots;
        public int mShotsOnTarget;
        public decimal mShotPercentage;
        public int mFouls;
        public decimal mFoulsPerMatch;
        public int mCorners;
        public decimal mCornersPerMatch;
        public int mYellowCards;
        public decimal mYellowsPerMatch;
        public int mRedCards;
        public decimal mRedsPerMatch;

        public Standing(int _standing, string _team, int _matches, int _wins, int _draws, int _losses, int _goalsFor, int _goalsAgainst,
            int _goalDiff, int _points, int _shots, int _shotsOnTarget, int _fouls, int _corners, int _yellows, int _reds)
        {
            mStanding = _standing;
            mTeamName = _team;
            mMatchesPlayed = _matches;
            mWins = _wins;
            mDraws = _draws;
            mLosses = _losses;
            mGoalsFor = _goalsFor;
            mGoalsAgainst = _goalsAgainst;
            mGoalDifferential = _goalDiff;
            mPoints = _points;
            mShots = _shots;
            mShotsOnTarget = _shotsOnTarget;
            mFouls = _fouls;
            mCorners = _corners;
            mYellowCards = _yellows;
            mRedCards = _reds;
        }
    }
}
