﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace footballData
{
    class Match
    {
        public string mDivision;
        public string mDate;
        public string mHomeTeam;
        public string mAwayTeam;
        public int mFullTimeHomeGoals;
        public int mFullTimeAwayGoals;
        public char mFullTimeResult;
        public int mHalfTimeHomeGoals;
        public int mHalfTimeAwayGoals;
        public char mHalfTimeResult;
        public string mReferee;
        public int mHomeShots;
        public int mAwayShots;
        public int mHomeShotsOnTarget;
        public int mAwayShotsOnTarget;
        public int mHomeFoulsCommitted;
        public int mAwayFoulsCommitted;
        public int mHomeCorners;
        public int mAwayCorners;
        public int mHomeYellowCards;
        public int mAwayYellowCards;
        public int mHomeRedCards;
        public int mAwayRedCards;

        public Match(string _div, string _date, string _home, string _away, int _ftHomeGoals, int _ftAwayGoals,
            char _ftResult, int _htHomeGoals, int _htAwayGoals, char _htResult, string _ref, int _homeShots, int _awayShots,
            int _homeShotsOT, int _awayShotsOT, int _homeFoulsCommitted, int _awayFoulsCommitted, int _homeCorners, int _awayCorners,
            int _homeYellows, int _awayYellows, int _homeReds, int _awayReds)
        {
            mDivision = _div;
            mDate = _date;
            mHomeTeam = _home;
            mAwayTeam = _away;
            mFullTimeHomeGoals = _ftHomeGoals;
            mFullTimeAwayGoals = _ftAwayGoals;
            mFullTimeResult = _ftResult;
            mHalfTimeHomeGoals = _htHomeGoals;
            mHalfTimeAwayGoals = _htAwayGoals;
            mHalfTimeResult = _htResult;
            mReferee = _ref;
            mHomeShots = _homeShots;
            mAwayShots = _awayShots;
            mHomeShotsOnTarget = _homeShotsOT;
            mAwayShotsOnTarget = _awayShotsOT;
            mHomeFoulsCommitted = _homeFoulsCommitted;
            mAwayFoulsCommitted = _awayFoulsCommitted;
            mHomeCorners = _homeCorners;
            mAwayCorners = _awayCorners;
            mHomeYellowCards = _homeYellows;
            mAwayYellowCards = _awayYellows;
            mHomeRedCards = _homeReds;
            mAwayRedCards = _awayReds;
        }
    }
}
