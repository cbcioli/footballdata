﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;

namespace footballData
{
    class MenuAction
    {
        List<Match> mMatchList;
        List<Standing> mTeamData;

        public MenuAction()
        {
            mMatchList = GetMatchList();
            mTeamData = GetTeamData(mMatchList);
        }

        List<Match> GetMatchList()
        {
            List<Match> matchList = new List<Match>();
            DatabaseConnection db = new DatabaseConnection();
            string select = "SELECT * FROM matchData_all";
            db.Open();
            MySqlCommand cmd = new MySqlCommand(select, db.GetConnection());
            MySqlDataReader rdr = cmd.ExecuteReader();

            while (rdr.Read())
            {
                matchList.Add(new Match(
                    rdr.GetString(0),
                    rdr.GetString(1),
                    rdr.GetString(2),
                    rdr.GetString(3),
                    rdr.GetInt32(4),
                    rdr.GetInt32(5),
                    rdr.GetChar(6),
                    rdr.GetInt32(7),
                    rdr.GetInt32(8),
                    rdr.GetChar(9),
                    rdr.GetString(10),
                    rdr.GetInt32(11),
                    rdr.GetInt32(12),
                    rdr.GetInt32(13),
                    rdr.GetInt32(14),
                    rdr.GetInt32(15),
                    rdr.GetInt32(16),
                    rdr.GetInt32(17),
                    rdr.GetInt32(18),
                    rdr.GetInt32(19),
                    rdr.GetInt32(20),
                    rdr.GetInt32(21),
                    rdr.GetInt32(22)));
            }
            rdr.Close();
            db.Close();
            return matchList;
        }

        Standing GetStandingFromMatch(Match m, int index)
        {
            int wins, losses, draws, goalDiff, points;
            if (index == 1)
            {

                if (m.mFullTimeResult == 'H')
                {
                    wins = 1;
                    losses = 0;
                    draws = 0;
                    points = 3;
                }
                else if (m.mFullTimeResult == 'A')
                {
                    wins = 0;
                    losses = 1;
                    draws = 0;
                    points = 0;
                }
                else
                {
                    wins = 0;
                    losses = 0;
                    draws = 1;
                    points = 1;
                }
                goalDiff = m.mFullTimeHomeGoals - m.mFullTimeAwayGoals;

                Standing s = new Standing(0, m.mHomeTeam, 38, wins, draws, losses, m.mFullTimeHomeGoals, m.mFullTimeAwayGoals,
                    goalDiff, points, m.mHomeShots, m.mHomeShotsOnTarget, m.mHomeFoulsCommitted, m.mHomeCorners, m.mHomeYellowCards, m.mHomeRedCards);
                return s;
            }
            else
            {
                if (m.mFullTimeResult == 'H')
                {
                    wins = 0;
                    losses = 1;
                    draws = 0;
                    points = 0;
                }
                else if (m.mFullTimeResult == 'A')
                {
                    wins = 1;
                    losses = 0;
                    draws = 0;
                    points = 3;
                }
                else
                {
                    wins = 0;
                    losses = 0;
                    draws = 1;
                    points = 1;
                }
                goalDiff = m.mFullTimeAwayGoals - m.mFullTimeHomeGoals;

                Standing s = new Standing(0, m.mAwayTeam, 38, wins, draws, losses, m.mFullTimeAwayGoals, m.mFullTimeHomeGoals,
                    goalDiff, points, m.mAwayShots, m.mAwayShotsOnTarget, m.mAwayFoulsCommitted, m.mAwayCorners, m.mAwayYellowCards, m.mAwayRedCards);
                return s;
            }
        }

        void UpdateStandings(Standing s, List<Standing> sList)
        {
            Standing existingStanding = sList.Find(l => l.mTeamName == s.mTeamName);
            if (existingStanding == null)
            {
                sList.Add(s);
            }
            else
            {
                existingStanding.mWins += s.mWins;
                existingStanding.mDraws += s.mDraws;
                existingStanding.mLosses += s.mLosses;
                existingStanding.mGoalsFor += s.mGoalsFor;
                existingStanding.mGoalsAgainst += s.mGoalsAgainst;
                existingStanding.mGoalDifferential += s.mGoalDifferential;
                existingStanding.mPoints += s.mPoints;
                existingStanding.mShots += s.mShots;
                existingStanding.mShotsOnTarget += s.mShotsOnTarget;
                existingStanding.mFouls += s.mFouls;
                existingStanding.mCorners += s.mCorners;
                existingStanding.mYellowCards += s.mYellowCards;
                existingStanding.mRedCards += s.mRedCards;
            }
        }

        List<Standing> GetTeamData(List<Match> matchList)
        {
            List<Standing> standingList = new List<Standing>();

            foreach (Match m in matchList)
            {
                Standing team1 = GetStandingFromMatch(m, 1);
                Standing team2 = GetStandingFromMatch(m, 2);
                UpdateStandings(team1, standingList);
                UpdateStandings(team2, standingList);

            }

            foreach (Standing s in standingList)
            {
                s.mShotPercentage = Math.Round(decimal.Divide(s.mShotsOnTarget, s.mShots), 3);
                s.mFoulsPerMatch = Math.Round(decimal.Divide(s.mFouls, s.mMatchesPlayed), 2);
                s.mCornersPerMatch = Math.Round(decimal.Divide(s.mCorners, s.mMatchesPlayed), 2);
                s.mYellowsPerMatch = Math.Round(decimal.Divide(s.mYellowCards, s.mMatchesPlayed), 2);
                s.mRedsPerMatch = Math.Round(decimal.Divide(s.mRedCards, s.mMatchesPlayed), 2);
            }

            return standingList;
        }

        List<Standing> CreateFinalTable()
        {
            mTeamData = mTeamData.OrderByDescending(s => s.mPoints)
            .ThenByDescending(s => s.mGoalDifferential)
            .ThenByDescending(s => s.mGoalsFor).ToList();

            for (int i = 0; i < mTeamData.Count; i++)
            {
                mTeamData[i].mStanding = i + 1;
            }

            return mTeamData;
        }

        public void ShowFinalTable()
        {
            List<Standing> finalTable = CreateFinalTable();
            string breaker = "----------------------------------------------------------";
            string headers = "| # |     TEAM     | MP | W | D | L | GF | GA | GD | PTS |";

            Console.WriteLine(breaker);
            Console.WriteLine(headers);
            Console.WriteLine(breaker);
            foreach (Standing s in finalTable)
            {
                if (s.mStanding > 17)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine($"|{s.mStanding.ToString().PadRight(3, ' ')}|{s.mTeamName.PadRight(14, ' ')}|{s.mMatchesPlayed.ToString().PadRight(4, ' ')}|{s.mWins.ToString().PadRight(3, ' ')}|{s.mDraws.ToString().PadRight(3, ' ')}|{s.mLosses.ToString().PadRight(3, ' ')}|{s.mGoalsFor.ToString().PadRight(4, ' ')}|{s.mGoalsAgainst.ToString().PadRight(4, ' ')}|{s.mGoalDifferential.ToString().PadRight(4, ' ')}|{s.mPoints.ToString().PadRight(5, ' ')}|");
                    Console.ResetColor();
                }
                else
                    Console.WriteLine($"|{s.mStanding.ToString().PadRight(3, ' ')}|{s.mTeamName.PadRight(14, ' ')}|{s.mMatchesPlayed.ToString().PadRight(4, ' ')}|{s.mWins.ToString().PadRight(3, ' ')}|{s.mDraws.ToString().PadRight(3, ' ')}|{s.mLosses.ToString().PadRight(3, ' ')}|{s.mGoalsFor.ToString().PadRight(4, ' ')}|{s.mGoalsAgainst.ToString().PadRight(4, ' ')}|{s.mGoalDifferential.ToString().PadRight(4, ' ')}|{s.mPoints.ToString().PadRight(5, ' ')}|");
            }
            Console.WriteLine(breaker);
            Console.ForegroundColor = ConsoleColor.Red;
            Console.Write("Red");
            Console.ResetColor();
            Console.Write(": Relegated\n");
        }

        List<Standing> CreateShotsTable()
        {
            mTeamData = mTeamData.OrderByDescending(s => s.mShotPercentage).ThenByDescending(s => s.mShots).ToList();

            for (int i = 0; i < mTeamData.Count; i++)
            {
                mTeamData[i].mStanding = i + 1;
            }

            return mTeamData;
        }

        public void ShowShotsTable()
        {
            List<Standing> shotsTable = CreateShotsTable();
            string breaker = "----------------------------------------------------------";
            string headers = "| # |     TEAM     | SHOTS | ON TARGET | ON TARGET RATIO |";

            Console.WriteLine(breaker);
            Console.WriteLine(headers);
            Console.WriteLine(breaker);

            foreach(Standing s in shotsTable)
            {
                Console.WriteLine($"|{s.mStanding.ToString().PadRight(3, ' ')}|{s.mTeamName.PadRight(14, ' ')}|{s.mShots.ToString().PadRight(7, ' ')}|{s.mShotsOnTarget.ToString().PadRight(11, ' ')}|{s.mShotPercentage.ToString().PadRight(17, ' ')}|");
            }
            Console.WriteLine(breaker);
        }

        Dictionary<string, List<Standing>> CreateCardsTable(string cardType)
        {
            if(cardType == "yellow")
                mTeamData = mTeamData.OrderByDescending(s => s.mYellowsPerMatch).ThenBy(s => s.mTeamName).ToList();
            else
                mTeamData = mTeamData.OrderByDescending(s => s.mRedsPerMatch).ThenBy(s => s.mTeamName).ToList();

            for (int i = 0; i < mTeamData.Count; i++)
            {
                mTeamData[i].mStanding = i + 1;
            }

            return new Dictionary<string, List<Standing>>() { { cardType, mTeamData } };
        }

        public void ShowCardsTable(string cardType)
        {
            Dictionary<string, List<Standing>> dictionary = CreateCardsTable(cardType);
            List<Standing> standingList = dictionary.FirstOrDefault().Value;

            if (dictionary.FirstOrDefault().Key == "yellow")
            {
                string breaker = "------------------------------------------------------------";
                string headers = "| # |     TEAM     | MATCHES | YELLOWS | YELLOWS PER MATCH |";
                Console.WriteLine(breaker);
                Console.WriteLine(headers);
                Console.WriteLine(breaker);
                foreach (Standing s in standingList)
                {
                    Console.WriteLine($"|{s.mStanding.ToString().PadRight(3, ' ')}|{s.mTeamName.PadRight(14, ' ')}|{s.mMatchesPlayed.ToString().PadRight(9, ' ')}|{s.mYellowCards.ToString().PadRight(9, ' ')}|{s.mYellowsPerMatch.ToString().PadRight(19, ' ')}");
                }
                Console.WriteLine(breaker);
            }
            else
            {
                string breaker = "------------------------------------------------------";
                string headers = "| # |     TEAM     | MATCHES | REDS | REDS PER MATCH |";
                Console.WriteLine(breaker);
                Console.WriteLine(headers);
                Console.WriteLine(breaker);
                foreach (Standing s in standingList)
                {
                    Console.WriteLine($"|{s.mStanding.ToString().PadRight(3, ' ')}|{s.mTeamName.PadRight(14, ' ')}|{s.mMatchesPlayed.ToString().PadRight(9, ' ')}|{s.mRedCards.ToString().PadRight(6, ' ')}|{s.mRedsPerMatch.ToString().PadRight(16, ' ')}");
                }
                Console.WriteLine(breaker);
            }
        }

        List<Standing> CreateCornersTable()
        {
            mTeamData = mTeamData.OrderByDescending(s => s.mCornersPerMatch).ThenByDescending(s => s.mCorners).ThenBy(s => s.mTeamName).ToList();

            for (int i = 0; i < mTeamData.Count; i++)
            {
                mTeamData[i].mStanding = i + 1;
            }

            return mTeamData;
        }

        public void ShowCornersTable()
        {
            List <Standing> cornersList = CreateCornersTable();

            string breaker = "--------------------------------------------------";
            string headers = "| # |     TEAM     | CORNERS | CORNERS PER MATCH |";
            Console.WriteLine(breaker);
            Console.WriteLine(headers);
            Console.WriteLine(breaker);

            foreach (Standing s in cornersList)
            {
                Console.WriteLine($"|{s.mStanding.ToString().PadRight(3, ' ')}|{s.mTeamName.PadRight(14, ' ')}|{s.mCorners.ToString().PadRight(9, ' ')}|{s.mCornersPerMatch.ToString().PadRight(19, ' ')}|");
            }
            Console.WriteLine(breaker);
        }
    }
}
